﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string vehicleId;
        public decimal transactionSum;
        public DateTime transactionTime;
        public decimal Sum { get => transactionSum; }
    }
}