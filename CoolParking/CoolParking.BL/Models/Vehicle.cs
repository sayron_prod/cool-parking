﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }
        static Regex template = new Regex(@"^[A-ZA-Z]{2}-[0-9]{4}-[A-ZA-Z]{2}$");
        public Vehicle(string id,VehicleType vehicleType,decimal balance)
        {
            if (balance < 0 || !IsValidId(id) || !Enum.IsDefined(typeof(VehicleType), vehicleType))
            {
                throw new ArgumentException();
            }
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public void AddBalance(decimal sum)
        {
            Balance += sum;
        }
        public static bool IsValidId(string id)
        {
            return template.IsMatch(id);
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random random = new Random();
            var builder = new StringBuilder(2);
            char offset = 'A';
            const int lettersOffset = 26;
            for (var i = 0; i < 2; i++)
            {
                var randomChar = (char)random.Next(offset, offset + lettersOffset);
                builder.Append(randomChar);
            }
            builder.Append('-');
            builder.Append(random.Next(0, 10000).ToString().PadLeft(4,'0'));
            builder.Append('-');
            for (var i = 0; i < 2; i++)
            {
                var randomChar = (char)random.Next(offset, offset + lettersOffset);
                builder.Append(randomChar);
            }
            return builder.ToString();
        }
    }
}