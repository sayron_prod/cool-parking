﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    class Parking
    {
        public decimal Balance { get; internal set; }
        public List<Vehicle> Vehicles = new List<Vehicle>();

        private static Parking instance = new Parking();
        public static Parking Instance { get { return instance; } }
        protected Parking()
        {
        }
    }
}