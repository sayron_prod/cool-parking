﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly decimal StartParkingBalance = 0M;
        public static readonly int ParkingVolume = 10;
        public static readonly double PeriodOfWithdrawal = 5D;
        public static readonly double PeriodOfLog = 60D;
        public readonly static Dictionary<VehicleType, decimal> Prices = new Dictionary<VehicleType, decimal>
        {
            [VehicleType.PassengerCar] = 2M,
            [VehicleType.Truck] = 5M,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Motorcycle] = 1M,

        };
        public static readonly decimal FineKoff = 2.5M;
    }
}