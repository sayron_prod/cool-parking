﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; private set; }
        public LogService(string path)
        {
            LogPath = path;
        }
        public static void ClearLogFile(string path)
        {
            File.WriteAllText(path, "");
        }

        public string Read()
        {
            string result=String.Empty;
            if(!File.Exists(LogPath))
            {
                throw new InvalidOperationException();
            }
            try
            {
                using (StreamReader reader = new StreamReader(LogPath))
                {
                    result = reader.ReadToEnd();
                }
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return result;
        }

        public void Write(string logInfo)
        {
            try
            {
                if (!string.IsNullOrEmpty(logInfo))
                {
                    using (StreamWriter writer = new StreamWriter(LogPath, true))
                    {
                        writer.Write(logInfo + "\n");
                    }
                }
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException();
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
    }
}