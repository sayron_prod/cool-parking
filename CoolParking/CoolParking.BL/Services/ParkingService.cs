﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        List<TransactionInfo> transactionInfo = new List<TransactionInfo>();
        ITimerService withdrawTimer;
        ITimerService logTimer;
        ILogService logService;
        Parking parking = Parking.Instance;
        private readonly object fineLocker = new();
        public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
        {
            withdrawTimer = _withdrawTimer;
            logTimer = _logTimer;
            logService = _logService;

            logTimer.Interval = Settings.PeriodOfLog * 1000;
            logTimer.Elapsed += LogTimer_Elapsed;
            logTimer.Start();

            withdrawTimer.Elapsed += WithdrawTimer_Elapsed;
            withdrawTimer.Interval = Settings.PeriodOfWithdrawal * 1000;
            withdrawTimer.Start();
        }

        private void WithdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            
            foreach (Vehicle vehicle in parking.Vehicles)
            {
                decimal payPrice = Settings.Prices[vehicle.VehicleType];
                lock (fineLocker)
                {
                    decimal vehicleBalance = vehicle.Balance;
                    if (vehicleBalance < 0)
                    {
                        payPrice *= Settings.FineKoff;
                    }
                    else if (vehicleBalance < payPrice)
                    {
                        payPrice = vehicleBalance + (payPrice - vehicleBalance) * Settings.FineKoff;
                    }
                    vehicle.Balance -= payPrice;
                    parking.Balance += payPrice;
                    var trInfo = new TransactionInfo { transactionSum = payPrice, transactionTime = DateTime.Now, vehicleId = vehicle.Id };
                    transactionInfo.Add(trInfo);
                }
            }
        }

        private void LogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            string logText = String.Empty;
            if (transactionInfo.Count > 0)
            {
                var transactions = transactionInfo.ToArray();
                foreach (TransactionInfo transaction in transactions)
                {
                    logText += $"{transaction.transactionTime} {transaction.transactionSum} money withdrawn from vehicle with Id='{transaction.vehicleId}'.\n";
                }
            }
            logService.Write(logText);
            transactionInfo.Clear();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (!Vehicle.IsValidId(vehicle.Id))
            {
                throw new InvalidDataException();
            }
            if (parking.Vehicles.Exists(x => x.Id == vehicle.Id))
            {
                throw new ArgumentException();
            }
            if (parking.Vehicles.Count >= Settings.ParkingVolume)
            {
                throw new InvalidOperationException();
            }
            parking.Vehicles.Add(vehicle);
        }

        public void Dispose()
        {
            withdrawTimer.Stop();
            withdrawTimer.Dispose();
            logTimer.Stop();
            logTimer.Dispose();
            transactionInfo.Clear();
            parking.Vehicles.Clear();
            parking.Balance = 0;
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingVolume;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingVolume - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfo.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }
        public decimal GetMoneyEarnedForCurrentPeriod()
        {
            return GetLastParkingTransactions().Sum(tr => tr.Sum);
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!Vehicle.IsValidId(vehicleId))
            {
                throw new InvalidDataException();
            }
            Vehicle vehicle = parking.Vehicles.FirstOrDefault(r => r.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException();
            }
            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Vehicle have negative balance");
            }
            parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                if (sum < 0)
                {
                    throw new ArgumentException("Sum is negative");
                }
                if (!Vehicle.IsValidId(vehicleId))
                {
                    throw new ArgumentException("Invalid vehicle id");
                }
                Vehicle vehicle = parking.Vehicles.FirstOrDefault(r => r.Id == vehicleId);
                vehicle.AddBalance(sum);
            }
            catch (NullReferenceException)
            {
                throw new ArgumentException($"Not exist vehicle with id {vehicleId}");
            }
        }
    }
}