﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Models
{
    class Vehicle
    {
        public string id { get; set; }
        public int vehicleType { get; set; }
        public decimal balance { get; set; }
    }
}
