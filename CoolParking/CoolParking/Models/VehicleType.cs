﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.

namespace CoolParking.Models
{
    public enum VehicleType
    {
        PassengerCar=1,
        Truck,
        Bus,
        Motorcycle
    }
}
