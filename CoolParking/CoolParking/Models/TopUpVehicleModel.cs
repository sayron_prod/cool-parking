﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Models
{
    public class TopUpVehicleModel
    {
        public string id { get; set; }
        public decimal Sum { get; set; }
    }
}
