﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Models
{
    class TransactionInfo
    {
        public string vehicleId { get; set; }
        public DateTime transactionDate { get; set; }
        public decimal sum { get; set; }
    }
}
