﻿using CoolParking.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParking
{
    class ParkingClient
    {
        string url = "https://localhost:44344/api";
        HttpClient client;
        public ParkingClient()
        {
            client = new HttpClient();
        }
        public decimal GetBalance()
        {
            return GetResult<decimal>($"{url}/parking/balance");
        }

        public int GetFreePlaces()
        {
            return GetResult<int>($"{url}/parking/freePlaces");
        }

        public int GetCapacity()
        {
            return GetResult<int>($"{url}/parking/capacity");
        }
        public decimal GetMoneyEarnedForCurrentPeriod()
        {
            return GetResult<decimal>($"{url}/parking/earnedmoney");
        }
        public T GetResult<T>(string url)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(url)
            };
            HttpResponseMessage response = client.Send(request);
            if (response.IsSuccessStatusCode)
            {
                var responseBody = response.Content.ReadAsStringAsync().Result;
                if(typeof(T) ==typeof(string))
                {
                    return (T)(object)responseBody;
                }
                return JsonSerializer.Deserialize<T>(responseBody);
            }
            else
            {
                throw new Exception($"{response.StatusCode} {response.Content.ReadAsStringAsync().Result}");
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return GetResult<TransactionInfo[]>($"{url}/transactions/last");
        }

        public string ReadFromLog()
        {
            try
            {
                return GetResult<string>($"{url}/transactions/all");
            }
            catch (Exception e)
            {
                if (e.Message.StartsWith("404"))
                {
                    throw new FileNotFoundException();
                }
                else
                {
                    throw new Exception(e.Message);
                }
            }
        }
        public Vehicle[] GetVehicles()
        {
            return GetResult<Vehicle[]>($"{url}/vehicles");
        }
        public Vehicle AddVehicle(Vehicle vehicle)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri($"{url}/vehicles"),
                Content=new StringContent(JsonSerializer.Serialize(vehicle),Encoding.UTF8, "application/json"),
            };
            HttpResponseMessage response = client.Send(request);
            if (response.IsSuccessStatusCode)
            {
                var responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonSerializer.Deserialize<Vehicle>(responseBody);
            }
            else
            {
                throw new Exception($"{response.Content.ReadAsStringAsync().Result}");
            }
        }
        public void RemoveVehicle(string id)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{url}/vehicles/{id}"),
            };
            HttpResponseMessage response = client.Send(request);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"{response.Content.ReadAsStringAsync().Result}");
            }
        }
        public Vehicle TopUpVehicle(string id, decimal sum)
        {
            TopUpVehicleModel model = new TopUpVehicleModel { id = id, Sum = sum };
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Put,
                RequestUri = new Uri($"{url}/transactions/topUpVehicle"),
                Content = new StringContent(JsonSerializer.Serialize(model), Encoding.UTF8, "application/json"),
            };

            HttpResponseMessage response = client.Send(request);
            if (response.IsSuccessStatusCode)
            {
                var responseBody = response.Content.ReadAsStringAsync().Result;
                return JsonSerializer.Deserialize<Vehicle>(responseBody);
            }
            else
            {
                throw new Exception($"{response.Content.ReadAsStringAsync().Result}");
            }
        }
        public void ClearLogFile()
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{url}/settings/logfile"),
            };
            HttpResponseMessage response = client.Send(request);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"{response.Content.ReadAsStringAsync().Result}");
            }
        }
    }
}
