﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CoolParking.Models;

namespace CoolParking
{
    class Menu
    {
        ParkingClient ParkingClient;
        public Menu()
        {
            ParkingClient = new ParkingClient();
        }
        public void Show()
        {
            Console.Clear();
            Console.WriteLine("1. Current parking balance");
            Console.WriteLine("2. Amount of money earned for the current period.");
            Console.WriteLine("3. Amount free/occupied parking places.");
            Console.WriteLine("4. All Parking Transactions for the current period.");
            Console.WriteLine("5. Transaction history.");
            Console.WriteLine("6. List of Vehicles located in the Parking.");
            Console.WriteLine("7. Add Vehicles in the Parking");
            Console.WriteLine("8. Remove the Vehicle from Parking.");
            Console.WriteLine("9. Top up the balance of Vehicle");
            Console.WriteLine("10. Clear log file");
            Console.WriteLine("0. End and Exit");
        }

        public void ClearLogFile()
        {
            try
            {
                ParkingClient.ClearLogFile();
                Print("Success");
            }
            catch (Exception e)
            {
                Print(e.Message);
            }
        }

        public void ShowAmount()
        {
            try
            {
                Print($"Amount of money earned for the current period - {ParkingClient.GetMoneyEarnedForCurrentPeriod()}");
            }
            catch (Exception e)
            {
                Print(e.Message);
            }
        }

        public int GetCommand()
        {
            int result;
            return Int32.TryParse(Console.ReadLine(), out result) ? result : -1;
        }
        public void AddVenicleToParking()
        {
            try
            {
                Console.WriteLine("Enter your vehicle number");
                string idNumber = Console.ReadLine();

                Console.WriteLine("Choose type of your vehicle");
                Console.WriteLine("1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle");
                int typeCar;
                VehicleType type;
                if (!Int32.TryParse(Console.ReadLine(), out typeCar) || !Enum.TryParse<VehicleType>(typeCar.ToString(), out type))
                {
                    ShowIncorrectError();
                    return;
                }

                Console.WriteLine("Enter your balance");
                decimal balanceUser;
                if (!Decimal.TryParse(Console.ReadLine(), out balanceUser))
                {
                    ShowIncorrectError();
                    return;
                }

                Vehicle vehicle = new Vehicle { id = idNumber, vehicleType = (int)type, balance = balanceUser };
                var result=ParkingClient.AddVehicle(vehicle);
                Console.WriteLine($"{result.id} {result.vehicleType} {result.balance}");
                Print("Success");
            }
            catch (Exception e)
            {
                Print(e.Message);
            }
        }
        public void RemoveVehicle()
        {
            try
            {
                Console.WriteLine("Enter vehicle id");
                string userVehicleIdToRemove = Console.ReadLine();
                ParkingClient.RemoveVehicle(userVehicleIdToRemove);
                Print("Success");
            }
            catch (Exception e)
            {
                Print(e.Message);
            }
        }
        public void TopUpVehicle()
        {
            try
            {
                Console.WriteLine("Enter vehicle id");
                string userVehicleId = Console.ReadLine();
                Console.WriteLine("The amount of money you need to add");
                decimal userMoney;
                if (!Decimal.TryParse(Console.ReadLine(), out userMoney))
                {
                    ShowIncorrectError();
                    return;
                }
                var result=ParkingClient.TopUpVehicle(userVehicleId, userMoney);
                Console.WriteLine($"{result.id} {result.vehicleType} {result.balance}");
                Print("Success");
            }
            catch (Exception e)
            {
                Print(e.Message);
            }
        }
        public void ShowLogFile()
        {
            try
            {
                Print("Transactions.log\n" + ParkingClient.ReadFromLog());
            }
            catch (FileNotFoundException)
            {
                Print("File not found");
            }
            catch(Exception e)
            {
                Print(e.Message);
            }
        }
        public void ShowVehiclesOnParking()
        {
            try
            {
                string tempMessage = "Vehicles in the parking:\n";
                foreach (var item in ParkingClient.GetVehicles())
                {
                    tempMessage += $"{item.id} {(VehicleType)item.vehicleType} {item.balance}\n";
                }
                Print(tempMessage);
            }catch (Exception e)
            {
                Print(e.Message);
            }
        }
        public void ShowLastParkingTransactions()
        {
            try
            {
                string tempMessage = "All transaction log for last period:\n";
                foreach (var transaction in ParkingClient.GetLastParkingTransactions())
                {
                    tempMessage += $"{transaction.transactionDate} {transaction.sum} money withdrawn from vehicle with Id= '{transaction.vehicleId}'.\n";
                }
                Print(tempMessage);
            }catch(Exception e)
            {
                Print(e.Message);
            }
        }
        public void ShowFreeOccupied()
        {
            try
            {
                int freePlaces = ParkingClient.GetFreePlaces();
                string tempMessage = $"Free parking spaces - {freePlaces}\nOccupied parking spaces - {ParkingClient.GetCapacity() - freePlaces}";
                Print(tempMessage);
            }catch(Exception e)
            {
                Print(e.Message);
            }
        }
        public void ShowCurrentBalance()
        {
            try
            {
                Print($"Current parking balance - {ParkingClient.GetBalance()}");
            }catch(Exception e)
            {
                Print(e.Message);
            }
        }
        public void Print(string message)
        {
            Console.Clear();
            Console.WriteLine(message);
            Console.WriteLine("Press any button to continue...");
            Console.ReadKey(false);
        }
        public void ShowIncorrectError()
        {
            Print("Soory, incorrect incoming data. Try again!");
        }
    }
}
