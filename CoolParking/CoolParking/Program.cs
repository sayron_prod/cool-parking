﻿using System;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            bool notExit = true;
            while (notExit)
            {
                menu.Show();
                switch (menu.GetCommand())
                {
                    case 1:
                        menu.ShowCurrentBalance();
                        break;
                    case 2:
                        menu.ShowAmount();
                        break;
                    case 3:
                        menu.ShowFreeOccupied();
                        break;
                    case 4:
                        menu.ShowLastParkingTransactions();
                        break;
                    case 5:
                        menu.ShowLogFile();
                        break;
                    case 6:
                        menu.ShowVehiclesOnParking();
                        break;
                    case 7:
                        menu.AddVenicleToParking();
                        break;
                    case 8:
                        menu.RemoveVehicle();
                        break;
                    case 9:
                        menu.TopUpVehicle();
                        break;
                    case 10:
                        menu.ClearLogFile();
                        break;
                    case 0:
                        notExit = false;
                        break;
                    default:
                        menu.ShowIncorrectError();
                        break;
                }
            }
        }
    }
}
