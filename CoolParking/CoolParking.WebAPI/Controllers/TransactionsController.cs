﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionsController : ControllerBase
    {
        private ParkingService parkingService;

        public TransactionsController(ParkingService _parkingService)
        {
            parkingService = _parkingService;
        }
        [HttpGet]
        [Route("last")]
        public ActionResult<TransactionInfoModel[]> GetLast()
        {
            var result = new List<TransactionInfoModel>();
            foreach (var item in parkingService.GetLastParkingTransactions())
            {
                result.Add(new TransactionInfoModel
                {
                    sum=item.Sum,
                    transactionDate=item.transactionTime,
                    vehicleId=item.vehicleId
                });
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<string> GetAll()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }catch(InvalidOperationException)
            {
                return NotFound("File not found");
            }
        }
        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult Post([FromBody] TopUpVehicleModel topUpVehicleModel)
        {
            if (topUpVehicleModel == null)
            {
                return BadRequest("Invalid data");
            }
            try
            {
                parkingService.TopUpVehicle(topUpVehicleModel.id, topUpVehicleModel.Sum);
                return Ok(parkingService.GetVehicles().FirstOrDefault(x=>x.Id==topUpVehicleModel.id));
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}

