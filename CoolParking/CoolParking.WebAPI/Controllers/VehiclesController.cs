﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public class VehiclesController : ControllerBase
    {
        private ParkingService parkingService;

        public VehiclesController(ParkingService _parkingService)
        {
            parkingService = _parkingService;
        }
        [HttpGet]
        public ActionResult<Vehicle[]> Get()
        {
            return Ok(parkingService.GetVehicles().ToArray());
        }
        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            var vehicle = parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (vehicle == null)
            {
                return NotFound($"Not exist vehicle with id {id}");
            }
            return Ok(vehicle);
        }
        [HttpPost]
        public ActionResult Post([FromBody]Vehicle vehicle)
        {
            if (vehicle == null)
            {
                return BadRequest("This is not a vehicle");
            }
            try
            {
                parkingService.AddVehicle(vehicle);
                return new ObjectResult(vehicle) { StatusCode = StatusCodes.Status201Created };
            }
            catch (ArgumentException)
            {
                return BadRequest("Already exist");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Parking is full");
            }
            catch (InvalidDataException)
            {
                return BadRequest("Invalid vehicle id");
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            try
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (InvalidDataException)
            {
                return BadRequest("Invalid vehicle id");
            }
            catch (ArgumentNullException)
            {
                return NotFound("Vehicle not exist");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Vehicle have negative balance");
            }
        }
    }
}

