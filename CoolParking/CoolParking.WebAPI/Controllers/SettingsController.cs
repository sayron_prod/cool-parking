﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/settings")]
    public class SettingsController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public SettingsController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        [HttpDelete]
        [Route("logfile")]
        public ActionResult Delete()
        {
            try
            {
                LogService.ClearLogFile(Configuration.GetValue<string>("Settings:LogPath"));
                return Ok();
            }catch(Exception e)
            {
                return Problem(e.Message);
            }
        }
    }
}

