﻿using CoolParking.BL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/parking")]
    public class ParkingController : ControllerBase
    {
        private ParkingService parkingService;

        public ParkingController(ParkingService _parkingService)
        {
            parkingService = _parkingService;
        }

        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetBalnce()
        {
            return Ok(parkingService.GetBalance());
        }
        [HttpGet]
        [Route("earnedmoney")]
        public ActionResult<decimal> GetEarnedMoney()
        {
            return Ok(parkingService.GetMoneyEarnedForCurrentPeriod());
        }
        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }
        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }
    }
}
