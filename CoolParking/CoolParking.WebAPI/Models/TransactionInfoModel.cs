﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class TransactionInfoModel
    {
        public string vehicleId { get; set; }
        public decimal sum { get; set; }
        public DateTime transactionDate { get; set; }
    }
}
